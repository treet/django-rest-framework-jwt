# REST framework JWT Auth

------------------------------------------------------------------------

## Notice

This project is currently unmaintained. [Check #484 for more details and
suggested
alternatives](https://github.com/jpadilla/django-rest-framework-jwt/issues/484).

------------------------------------------------------------------------

[![build-status-image](https://secure.travis-ci.org/jpadilla/django-rest-framework-jwt.svg?branch=master)](http://travis-ci.org/jpadilla/django-rest-framework-jwt?branch=master)
[![pypi-version](https://img.shields.io/pypi/v/djangorestframework-jwt.svg)](https://pypi.python.org/pypi/djangorestframework-jwt)

**JSON Web Token Authentication support for Django REST Framework**

Full documentation for the project is available at
[docs](https://jpadilla.github.io/django-rest-framework-jwt/).

## Overview

This package provides [JSON Web Token
Authentication](http://tools.ietf.org/html/draft-ietf-oauth-json-web-token)
support for [Django REST framework](http://django-rest-framework.org/).

If you want to know more about JWT, check out the following resources:

-   DjangoCon 2014 - JSON Web Tokens
    [Video](https://www.youtube.com/watch?v=825hodQ61bg) \|
    [Slides](https://speakerdeck.com/jpadilla/djangocon-json-web-tokens)
-   [Auth with JSON Web
    Tokens](http://jpadilla.com/post/73791304724/auth-with-json-web-tokens)
-   [JWT.io](http://jwt.io/)

## Requirements

-   Python (3.8)
-   Django (4.2)
-   Django REST Framework (3.14)

## Installation

Install using `pip`\...

```bash
$ pip install djangorestframework-jwt
```

## Documentation & Support

Full documentation for the project is available at
[docs](https://jpadilla.github.io/django-rest-framework-jwt/).

You may also want to follow the [author](https://twitter.com/blimp) on
Twitter.
